package com.example.rm75859.servicelog;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;


public class DelayedMessageService extends IntentService {

    public static final String EXTRA_MESSAGE = "mensagem";

    public DelayedMessageService() {
        super("DelayedMessageService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        synchronized (this){
            try {
                wait(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            String text = intent.getStringExtra(EXTRA_MESSAGE);

            showText(text);
        }
    }

    private void showText(String text) {
        Log.v("Serviço", "A nossa mensagem é:" + text);


    }

}
